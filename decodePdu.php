<?php

class sms_pdu_decode {

	const NUMBER_TYPE_UNKNOWN = 1;
	const NUMBER_TYPE_INTERNATIONAL = 2;
	const NUMBER_TYPE_NATIONAL = 3;
	const NUMBER_TYPE_NETWORK = 4;
	const NUMBER_TYPE_SUBSCRIBER = 5;
	const NUMBER_TYPE_ALPHANUMERIC = 6;
	const NUMBER_TYPE_ABBREVIATED = 7;
	const NUMBER_TYPE_RESERVED = 8;

	const NUMBER_PLAN_UNKNOWN = 1;
	const NUMBER_PLAN_ISDN = 2;
	const NUMBER_PLAN_DATA = 3;
	const NUMBER_PLAN_TELEX = 4;
	const NUMBER_PLAN_NATIONAL = 5;
	const NUMBER_PLAN_PRIVATE = 6;
	const NUMBER_PLAN_ERMES = 7;
	const NUMBER_PLAN_RESERVED = 8;

	const MESSAGE_TEXT_UNCOMPRESSED = 0;
	const MESSAGE_TEXT_COMPRESSED = 1;

	const MESSAGE_ALPHABET_DEFAULT = 1;
	const MESSAGE_ALPHABET_8BITDATA = 2;
	const MESSAGE_ALPHABET_UCS2 = 3;
	const MESSAGE_ALPHABET_RESERVED = 4;

	const MESSAGE_CLASS_IMMEDIATE_DISPLAY = 0;
	const MESSAGE_CLASS_ME_SPECIFIC = 1;
	const MESSAGE_CLASS_SIM_SPECIFIC = 2;
	const MESSAGE_CLASS_TE_SPECIFIC = 3;
	
	const MESSAGE_PDU = 44;

	public $pdu;
	private $retr_next_char_pos = 0;

	private $smsc_len;
	public $smsc_number;
	public $smsc_number_type;
	public $smsc_number_plan;
	public $headerLength=0;
	public $timestamp=0;
	
	private $sender_number_len;
	static $sender_number_flag = false;
	public $sender_number;
	public $sender_number_type;
	public $sender_number_plan;

	public $message_compression;
	public $message_alphabet;
	public $message_class;
	public $pduHeader='';

	public $smsc_timestamp;
	public $protocol_identifier;
	public $user_data_len;
	public $user_data;
	public $time;
	public $date;
	//Header Parameters
	
	public $flag;
	public $uniqe;
	public $total_parts;
	public $parts_no;
	public $numberFlag=false;
	static $numbertype_flag=false;
	//
	public $lowerBits;
	public $upperBits;

	public function decode() {

		//Length of the SMSC information
		$this->smsc_len(); //Length of the SMSC information

		//Type of SMSC address
		$this->smsc_number_type();

		//Service center number
		$this->smsc_number();

		//First octet
		$this->first_octet();

		//Sender number length
		$this->sender_number_len();

		//Sender number type
		$this->sender_number_type();

		//Sender number
		$this->sender_number();

		//Protocol identifier
		$this->protocol_identifier();

		//Data coding scheme
		$this->data_coding_scheme();

		//SMSC Timestamp
		$this->smsc_timstamp();
		
		//Userdata length
		$this->user_data_length();
		
		//PDU_Header
		if($this->flag)
			$this->pdu_header();
		
		//Userdata
		//$this->decode_pdu();
		$this->user_data();

	}

	//Length of the SMSC information
	private function smsc_len() {

		$len = $this->retrieve_next_char(2);
		$this->smsc_len = self::number_len($len);
		//echo "sms_len ".$this->smsc_len."\n";

	}

	//SMSC number type
	private function smsc_number_type() {

		$type = $this->retrieve_next_char(2);
		$type_info = self::number_type($type);
		//echo $type."\n";
		$this->smsc_number_type = $type_info['type'];
		$this->smsc_number_plan = $type_info['plan'];

	}

	//SMSC number
	private function smsc_number() {

		$length = ($this->smsc_len * 2) - 2;
		//echo "\n $length \n";
		$number = $this->retrieve_next_char($length);
		
		$this->smsc_number = $this->number($number);
		//echo "\n$this->smsc_number\n";

	}

	//First octet
	private function first_octet() {

		$first_octet = $this->retrieve_next_char(2);
		//echo "\nFirst Octet:$first_octet\n";
		if(!strcmp($first_octet,'44')||!strcmp($first_octet,'64')||!strcmp($first_octet,'40'))
			$this->flag=TRUE;

	}

	//Sender number length
	private function sender_number_len() {

		$len = $this->retrieve_next_char(2);
		$this->sender_number_len = self::number_len($len);
		//echo "\n$this->sender_number_len\n";
		
	}

	//Sender number type
	private function sender_number_type() {

		$type = $this->retrieve_next_char(2);
		$type_info = self::number_type($type);
		//echo "\nType Info:$type_info\n";
		
		$this->sender_number_type = $type_info['type'];
		$this->sender_number_plan = $type_info['plan'];

	}

	//Sender number
	private function sender_number() {
		$num_flag = false;
		if($this->sender_number_len%2!=0){
			$number = $this->retrieve_next_char($this->sender_number_len+1);
			//self::$sender_number_flag = true;
			//$num_flag= true;
		}else{
			$number = $this->retrieve_next_char($this->sender_number_len);
		}
		if(ctype_digit(substr($number,0,-2))&& ctype_digit(substr($number,-1)) && (ctype_digit(substr($number,-2,1))|| 'F'==substr($number,-2,1)))
		  $this->sender_number = $this->number($number);
		else {
			$this->numberFlag=true;
			$this->sender_number = $this->decodPdu($number);
		}

		$this->sender_number = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $this->sender_number);	    	
	}

	//Protocol identifier
	private function protocol_identifier() {

		$this->protocol_identifier = $this->retrieve_next_char(2);

		if ($this->protocol_identifier != '00') {
			//throw new Exception("Protocol identifier \"{$protocol_identifier}\" not supported");
		}

	}

	//Data coding scheme
	private function data_coding_scheme() {

		//http://web.archive.org/web/20120714175243/http://dreamfabric.com/sms/dcs.html

		$data_coding_scheme = $this->retrieve_next_char(2);

		$data_coding_scheme_dec = hexdec($data_coding_scheme);

		$data_coding_scheme_bin = decbin($data_coding_scheme_dec);
		$data_coding_scheme_bin = str_pad($data_coding_scheme_bin, 8, '0', STR_PAD_LEFT);

		//If "General Data Coding" scheme
		if (preg_match('/^00(\d{1})(\d{1})(\d{2})(\d{2})$/', $data_coding_scheme_bin, $matches)) {

			$compression_code = $matches[1];
			//$reseved_code = $matches[2];
			$alphabet_code = $matches[3];
			$class_code = $matches[4];

			//Compression

			$this->message_compression = ($compression_code == '1') ? self::MESSAGE_TEXT_COMPRESSED : self::MESSAGE_TEXT_UNCOMPRESSED;

			//Alphabet

			$alphabet_resolve = array(
					'00' => self::MESSAGE_ALPHABET_DEFAULT,
					'01' => self::MESSAGE_ALPHABET_8BITDATA,
					'10' => self::MESSAGE_ALPHABET_UCS2,
					'11' => self::MESSAGE_ALPHABET_RESERVED,
			);

			if (!isset($alphabet_resolve[$alphabet_code])) {
				throw new Exception("Message alphabet \"{$alphabet_code}\" not recognised");
			}

			$this->message_alphabet = $alphabet_resolve[$alphabet_code];

			//Class

			$class_resolve = array(
					'00' => self::MESSAGE_CLASS_IMMEDIATE_DISPLAY,
					'01' => self::MESSAGE_CLASS_ME_SPECIFIC,
					'10' => self::MESSAGE_CLASS_SIM_SPECIFIC,
					'11' => self::MESSAGE_CLASS_TE_SPECIFIC,
			);

			if (!isset($class_resolve[$class_code])) {
				throw new Exception("Message class \"{$class_code}\" not recognised");
			}

			$this->message_class = $class_resolve[$class_code];

		} else {
			//throw new Exception("Data coding scheme not supported \"{$data_coding_scheme_bin}\"");
		}

	}

	//SMSC Timestamp
	private function smsc_timstamp() {

		 $date_time = $this->retrieve_next_char(14);
		 $this->timestamp=$date_time;
		 //echo "\nDate time:".$date_time."\n";
		$date_time = $this->number($date_time);
		$date=substr($date_time,0,6);
		$time = substr($date_time,6,12);
		$date1 = substr($date,0,2);
		$date2 = substr($date,2,2);
		$date3 = substr($date,4,6); 
		$date = "20".$date1."-".$date2."-".$date3;
		$this->date= $date;
		$time1 = substr($time,0,2);
		$time2 = substr($time,2,2);
		$time3 = substr($time,4,6);
		$time = $time1.":".$time2.":".$time3;
		$this->time = substr($time,0,8);
	}
	
	//Pdu Header Length
	private function pdu_header(){
		$pduLen = $this->retrieve_next_char(2);
		$p1=$pduLen;
		$pduLen = hexdec($pduLen);
		$this->headerLength=$pduLen;
		$le=$pduLen*2-4;
		$this->uniqe = $this->retrieve_next_char($le);
		/*$typ = substr($this->uniqe,0,2);
		$next = substr($this->uniqe,2,2);
		$uni = substr($this->uniqe,4,$next);*/
		
		$this->total_parts = $this->retrieve_next_char(2);
		$this->uniqe = $this->uniqe.$this->total_parts;
		$this->parts_no = $this->retrieve_next_char(2);
		$this->pduHeader = $p1.$this->uniqe.$this->parts_no;
	}
	//User Data Length
	private function user_data_length() {
		//echo "\n";
		$user_data_len = $this->retrieve_next_char(2);

		$user_data_len_dec = hexdec($user_data_len);

		if (!( ($user_data_len_dec >= 0) && ($user_data_len_dec <= 160) )) {
			throw new Exception("Message length \"{$user_data_len}\" invalid");
			/*if($user_data_len_dec >= 160){
				if(hexdec(substr($user_data_len,0,1))>=160)
					$this->retr_next_char_pos=$this->retr_next_char_pos-2;
			}*/
		}

		$this->user_data_len = $user_data_len_dec;

	}

	//User Data
	private function user_data() {
 		$this->numberFlag=false;
		$this->user_data= $this->decodPdu($this->pdu,$this->pduHeader);
		$this->user_data = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $this->user_data);	    	
	}

	//Retrieve characters from PDU data
	private function retrieve_next_char($length) {
		
		$characters = substr($this->pdu, $this->retr_next_char_pos, $length);
		$this->retr_next_char_pos = $this->retr_next_char_pos + $length;
		//echo "Charcter position=".$this->retr_next_char_pos."\n";
		$characters_len = strlen($characters);
		if ($characters_len != $length) {
			throw new Exception("Expected length \"{$length}\"does not match available data length \"{$characters_len}\"");
		}
		//echo "\n".$this->retr_next_char_pos."\n";
		return $characters;

	}

	//Number length
	static private function number_len($len) {

		$len_dec = hexdec($len);
		if (!( ($len_dec > 0) && ($len_dec < 50) )) {
			throw new Exception("Number length \"{$smsc_len_dec}\" invalid");
		}
		return $len_dec;

	}

	//Number type
	static private function number_type($type) {

		//http://web.archive.org/web/20120714175848/http://dreamfabric.com/sms/type_of_address.html

		$type_dec = hexdec($type);

		$type_bin = decbin($type_dec);
		//echo "\nType Bin:$type_bin\n";
		//$type_bin = str_pad($type_bin, 8, '0', STR_PAD_LEFT);

		//7: Always set to 1
		//if (!($type_dec & bindec('1000000'))) {
		//}

		if (!preg_match('/^\d{1}(\d{3})(\d{4})$/', $type_bin, $matches)) {
			
			throw new Exception("Number type match failed \"{$type_bin}\"");
			
		}
		//print_r($matches);
		if ($matches[1] != 1) {
			//print_r($matches);
			//throw new Exception("Number type digit 7 not set to 1 \"{$type_bin}\"");
			//self::$numbertype_flag=TRUE;
		}

		$type_code = $matches[1];
		$plan_code = $matches[2];
		
		$type_resolve = array(
				'000' => self::NUMBER_TYPE_UNKNOWN,
				'001' => self::NUMBER_TYPE_INTERNATIONAL,
				'010' => self::NUMBER_TYPE_NATIONAL,
				'011' => self::NUMBER_TYPE_NETWORK,
				'100' => self::NUMBER_TYPE_SUBSCRIBER,
				'101' => self::NUMBER_TYPE_ALPHANUMERIC,
				'110' => self::NUMBER_TYPE_ABBREVIATED,
				'111' => self::NUMBER_TYPE_RESERVED,
		);

		if (!isset($type_resolve[$type_code])) {
			throw new Exception("Number type \"{$type_code}\" not recognised");
		}

		$plan_resolve = array(
				'0000' => self::NUMBER_PLAN_UNKNOWN,
				'0001' => self::NUMBER_PLAN_ISDN,
				'0011' => self::NUMBER_PLAN_DATA,
				'0100' => self::NUMBER_PLAN_TELEX,
				'1000' => self::NUMBER_PLAN_NATIONAL,
				'1001' => self::NUMBER_PLAN_PRIVATE,
				'1010' => self::NUMBER_PLAN_ERMES,
				'1111' => self::NUMBER_PLAN_RESERVED,
		);

		if (!isset($plan_resolve[$plan_code])) {
			throw new Exception("Number plan \"{$plan_code}\" not recognised");
		}

		//if (!in_array($type_resolve[$type_code], array(self::NUMBER_TYPE_UNKNOWN, self::NUMBER_TYPE_INTERNATIONAL, self::NUMBER_TYPE_NATIONAL))) {
		if ($type_code == '101') {
			if ($plan_resolve[$plan_code] != self::NUMBER_PLAN_UNKNOWN) {
				//throw new Exception("Number plan (\"{$plan_code}\") must be 'unknown' for specified number type \"{$type_code}\"");
			}
		}

		$return = array(
				'type' => $type_resolve[$type_code],
				'plan' => $plan_resolve[$plan_code],
		);

		return $return;

	}

	//Number
	static private function number($number) {

		//Phone Number
		$phone_number = '';
			$telephone_chunks = str_split($number, 2);
			if(self::$numbertype_flag){
				foreach ($telephone_chunks as $chunk) {
					$phone_number = chr(hexdec($chunk));
					//$phone_number .= $chunk_rev;
				}
				//$phone_number.=(hexdec($number));
			}else{
				foreach ($telephone_chunks as $chunk) {
					$chunk_rev = strrev($chunk);
					$phone_number .= $chunk_rev;
				}
			$phone_number = rtrim($phone_number, 'F');
			}
			//echo "\n Sender Number: $phone_number";
			return $phone_number;
	}
	
	public function decodPdu($pdu_msg,$header=null){
		if($this->numberFlag)
			$str=$pdu_msg;
		else
			$str = substr($pdu_msg,$this->retr_next_char_pos);
		
		$str = $header.$str;
		$tr = str_split($str,2);
		$i=0;
		$dec = Array();
		
		foreach ($tr as $hexstr){
		
			$dec[$i]=decbin(hexdec($hexstr));
			$i++;
		}
		$i=0;
		
		for($i=0;$i<count($dec);$i++){
		
			$dec[$i]=str_pad($dec[$i],8,'0',STR_PAD_LEFT);
		}
		//print_r($dec);
		$bin='';
		for($i=0;$i<count($dec);$i++){
			$bin=$dec[$i].$bin;
		}
		$bin=strrev($bin);
		$bi = str_split($bin,7);
		for($i=0;$i<count($bi);$i++){
			$bi[$i]=strrev($bi[$i]);
		}
		//print_r($bi);
		
		for($i=0;$i<count($bi);$i++){
			$bi[$i]=chr(bindec($bi[$i]));
		}
		//print_r($bi);
		$text='';
		for($i=0;$i<count($bi);$i++){
			$text.=$bi[$i];
		}
		//echo $l;
		if($this->numberFlag)
			return $text;
		if($this->flag){
			
			$len = ceil((8*($this->headerLength))/7);
			return substr($text,-($this->user_data_len-$len));
		}else{
			return $text;
		}
	}
	
}

