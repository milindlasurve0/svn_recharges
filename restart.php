<?php
include "config.php";
$ret = shell_exec("ps ax  | grep recharges/restart.php | wc -l");
if($ret > 3){
	exit;
}
echo "Killing recharge modem script\n";

$processes = explode("\n",shell_exec("ps ax  | awk '/start.php start/ {print $1}'"));
foreach($processes as $process){
	$process = trim($process);
	if(!empty($process))shell_exec("kill $process");
}

$processes = explode("\n",shell_exec("ps ax  | awk '/start.php receive/ {print $1}'"));
foreach($processes as $process){
	$process = trim($process);
	if(!empty($process))shell_exec("kill $process");
}

$processes = explode("\n",shell_exec("ps ax  | awk '/start.php autocheck/ {print $1}'"));
foreach($processes as $process){
	$process = trim($process);
	if(!empty($process))shell_exec("kill $process");
}

$processes = explode("\n",shell_exec("ps ax  | awk '/start.php recheck/ {print $1}'"));
foreach($processes as $process){
	$process = trim($process);
	if(!empty($process))shell_exec("kill $process");
}

$processes = explode("\n",shell_exec("ps ax  | awk '/redis_request_processor.php/ {print $1}'"));
foreach($processes as $process){
	$process = trim($process);
	if(!empty($process))shell_exec("kill $process");
}

$processes = explode("\n",shell_exec("ps ax  | awk '/redis_prev_requests_processor.php/ {print $1}'"));
foreach($processes as $process){
	$process = trim($process);
	if(!empty($process))shell_exec("kill $process");
}

shell_exec("nohup sh ".DOCUMENT_ROOT."recharges/redis_process_definer.sh > /dev/null 2> /dev/null & echo $!");

$ret = shell_exec("ps ax  | grep 'recharges/start.php recharge ' | wc -l");
$i = 0;
while($ret > 2 && $i < 10){
	sleep(5);
	$ret = shell_exec("ps ax  | grep 'recharges/start.php recharge ' | wc -l");
	$i++;
}
echo "Starting recharge modem script\n";
shell_exec("nohup php ".DOCUMENT_ROOT."recharges/start.php start > /dev/null 2> /dev/null & echo $!");

?>