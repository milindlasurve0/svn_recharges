
CPATH='/var/log/recharges'
CHPATH='/mnt/logs'

## if directoy does not exist create it
if [ -d $CPATH ]; then
    cd $CPATH
else
    echo "---------------------"
    echo $CPATH 
    mkdir $CPATH
    cd $CPATH
fi

### get inside the log

###--- generate name for folder
DIR_NAME=$(date +%Y%m%d)

### ---------- create directory of current date
make_dir=$(mkdir $DIR_NAME)

###---------- copy all log file
copy_path=$(mv $CHPATH/* $DIR_NAME/.)

### -------- archive folder
tar -zcvf $DIR_NAME.tar.gz $DIR_NAME

## -------- delete folder after archival
delete_folder=$(rm -r $DIR_NAME)

## ------- delete 30 days old archival
CDATE=`date --date='today' +%Y%m%d`
ARCTHRESHOLD=30
rmfilelist=`ls *.tar.gz`
for logf in $rmfilelist
  do
    FDATE=`echo $logf  | awk -F'.' '{ n=split($0,array,"_")} END{ print $n}'`
    fileage=$(($(($(date -d $CDATE "+%s") - $(date -d $FDATE "+%s"))) / 86400))
    if [ $fileage -gt $ARCTHRESHOLD ]; then
        echo "removing " $logf
        rm -f $logf
    fi
  done

ARCTHRESHOLD=15
cd /root
rmfilelist=`ls *.sql`
for logf in $rmfilelist
  do
    FDATE=`echo $logf  | awk -F'.' '{ n=split($0,array,"_")} END{ print $n}'`
    fileage=$(($(($(date -d $CDATE "+%s") - $(date -d $FDATE "+%s"))) / 86400))
    if [ $fileage -gt $ARCTHRESHOLD ]; then
        echo "removing " $logf
        rm -f $logf
    fi
  done

find /root/recharges_*.sql -mtime +15 -exec rm {} \;
