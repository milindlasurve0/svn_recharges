<?php
/*#################################################################
 * socket handler to create, send and receive data via UDP/TCP socket
 *#################################################################
 */

class socket_handler
{
    public function send_msg_via_socket($data = array(),$socktype="UDP"){
	try{
	    $content = $data['content'];
	    $ip = $data['ipaddr'];
	    $port = $data['port'];
	    if($socktype === "UDP"){
	        $sock = socket_create(AF_INET, SOCK_DGRAM, 0);
	        if(!socket_sendto($sock, $content , strlen($content) , 0 , $ip , $port)){
		    return FALSE;
	        }
	    }elseif($socktype === "TCP"){
	        $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                socket_connect($sock, $ip, $port);
                if(!socket_write($sock, $content, strlen($content))){
		    return FALSE;
	        }
	    }else{
                throw new Exception("socket type not defined");
	    }
	}catch(Exception $ex){
	    return FALSE;
	}
	return array('sock'=>$sock);	
    }

    public function create_socket($data = array(),$socktype="UDP"){
	try{
            $ip = $data['ipaddr'];
            $port = $data['port'];
            if($socktype === "UDP"){
	        $sock = socket_create(AF_INET, SOCK_DGRAM, 0);
	        socket_bind($sock, $ip , $port);
            }elseif($socktype === "TCP"){
                $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                socket_connect($sock, $ip, $port);   
            }
	}catch(Exception $ex){
	    return FALSE;
	}
	    return $sock;
    }

    public function create_socket_server_connection($data = array(),$socktype="UDP"){
        try{
	    $ip = $data['ipaddr'];
	    $port = $data['port'];
	    $sock = $this->create_socket($data,$socktype);
	    if(!$sock){
	        throw new Exception("socket not created !! ");
	    }
	}catch(Exception $ex){
	    $errorcode = socket_last_error();
	    $errormsg = socket_strerror($errorcode);
	    return FALSE;
	}
	return $sock;
    }

    public function create_socket_client_connection($data = array(),$socktype="UDP"){
       try{
	    $reply = "";
	    $sock = $data['sock'];
            if(!$sock){
                throw new Exception("socket not created");
            }
            if($socktype === "UDP"){
	        if(socket_recv ( $sock , $reply , 2045 , MSG_WAITALL ) === FALSE){
	            throw new Exception("socket error !!");
	        }
            }elseif($socktype === "TCP"){
                $reply = socket_read($sock, 2048);                
            }
	}catch(Exception $ex){
	    $errorcode = socket_last_error();
	    $errormsg = socket_strerror($errorcode);
	    return FALSE;
	}
	return $reply; 
    }

    public function receive_msg_via_socket($data = array(),$socktype="UDP"){
	$sock = $data['sock'];
	$buf = "";
	$remote_ip = "";
	$remote_port = "";
	try{
            if($socktype === "UDP"){
                $r = socket_recvfrom($sock, $buf, 2048, 0, $remote_ip, $remote_port);
            }elseif($socktype === "TCP"){
                $buf = socket_read($sock, 2048);
            }
	}catch(Exception $ex){
            print_r($ex);
	    return FALSE;
	}
	return array('message'=>$buf,'remote_ip'=>$remote_ip,'remote_port'=>$remote_port);
    }

    public function receive_all_message($data = array(),$socktype="UDP"){
        print_r($data);
	$sock = $data['sock'];
        if($socktype === "UDP"){
	    if(socket_recv($sock,$reply,2045,MSG_WAITALL) === FALSE){
		echo "no message received";
		return FALSE;	
  	    }
        }elseif($socktype === "TCP"){
            $reply = socket_read($sock, 2048);      
        }
        return array('message'=>$reply); 
    }
    
}
?>