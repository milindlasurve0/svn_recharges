/usr/sbin/dmidecode | egrep "ID: [A-Z|0-9|a-z]* _*|UUID" | tr -d "\t" > $1
/bin/cat /proc/cmdline | sed 's/ /\n/g' | grep "UUID" | awk -F'=' '{ print $NF }' >> $1
