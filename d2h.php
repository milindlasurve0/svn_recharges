<?php
class d2h{
	//curlMethod($device,$opr,$mobile,$amount,$type,$pin,$param,$scid,$devid,$retMobile);
	public function curlMethod($type,$device,$opr,$mobile=null,$amount=null,$pass,$param=null,$scid=null,$devid,$userName){
		$amount = intval($amount);
		$param = intval($param);
		$finalResult = array(
				'status'=>'',
				'out'=>""
		);
		$i=0;
		$cookie = '';
		$session = '';
		$expect = '';
		$location = '';
		$body = '';
		$postString='';
		$len=0;
		$curl = curl_init();
		while ($i<=7){
			//if($i==3)$i=4;
			logData("log_$devid.txt","<----------------------$i-------------------------------------------------------->");												//$useName,$pass,$customerID,$amount
			$expected = $this->getExpected($i,$expect,$cookie,$session,$location,$userName,$pass,$param,$amount,$len);
			logData("log_$devid.txt","ARRAY::".json_encode($expected));
			print_r($expected);
			
			curl_setopt($curl, CURLOPT_HTTPHEADER,$expected['header']);
			curl_setopt($curl, CURLOPT_URL,$expected['url']);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_HEADER,1);
			
			if($expected['type']==='POST'){
				
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $postString);
				
			}
			
			$result = curl_exec($curl);
			
			if($i==5)$finalResult['status']='success';
			
			$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
			
			$responseHeader = substr($result, 0, $header_size);
			
			$body = substr($result, $header_size);
			
			logData("log_$devid.txt","HeaderSize::$header_size");
			logData("log_$devid.txt","Header::$responseHeader");
			//logData('dishtv.txt',"BODY::".$body);
			
			$out = $this->htmlParse($body,$expected['xPath'][0]);
			
			$finalResult[] = $out;
			$finalResult['out']=$finalResult['out']."$i::".$out.":";
			if($i==6){
				$out = str_replace(array('\n','\r','\u00a0','  '),'',$out);
				$out = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $out);
				$finalResult['sms']=trim($out);
			}
			if(($expected['expected'] == trim($out) || strpos($out,$expected['expected'])!==false) && !empty($expected['expected'])){
				
				$headData = $this->getHeaderData($responseHeader);
				
				if(isset($headData['Cookie']))
					$cookie = $headData['Cookie'];
				if(isset($headData['sessionID']) && isset($headData['Cookie']))
					$session = $headData['sessionID'];
				if(isset($headData['Location'])){
					$location = $headData['Location'];
				}
													//($body,$find,$k,$userName,$pass,$customerID,$amount)
				$postString = $this->creatResources($body,$expected['xPath'],$i,$userName,$pass,$param,$amount);
				
				$len = strlen($postString);
				
				if($type==='Balance' && $i==2){
					$i=6;
					$finalResult['balance'] = $this->htmlParse($body,'//span[@id="CenterCPH_lblBalance"]');
					$finalResult['balance'] = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $finalResult['balance']);
					$finalResult['status'] = 'success';
					logData("log_$devid.txt","balance=".$finalResult['balance']);
				}
				
			}else{
				
				if($expected['expected']==$param)
					$finalResult['sms'] = $this->htmlParse($body,'//div[@class="message"][@id="message"]');
				if($i>=2)$finalResult['status']='success';
				else $finalResult['status']='failure';
				logData("log_$devid.txt","failed::$i");
				break;
				
			}
			
			logData("log_$devid.txt","cookie::$cookie::session::$session::location::$location");
			$i++;
		}
		
		curl_close($curl);
		return $finalResult;
		
	}
	
	
	
	public function getHeaderData($header){
		
		$a = explode("\n",$header);
		$c = array();
		$i=1;
		
		foreach ($a as $val){
			$b = explode(":",$val);
			if(trim($b[0])=='Set-Cookie' && $i==1)
			{
				$b[0] = 'sessionId';
				$i++;
			}
			$c[trim($b[0])]=trim($b[1]);
		}
		
		if(isset($c['Set-Cookie'])){
			$z = explode(";",$c['Set-Cookie']);
			$c['Cookie'] = $z[0];
		}
		
		if(isset($c['sessionId'])){
			$l = explode(";",$c['sessionId']);
			$c['sessionID'] = $l[0];
		}
		return $c;
	}
	
	
	public function htmlParse($page,$path){
		
		$dom = new DOMDocument();
		$dom->loadHTML($page);
		
		$xpath = new DOMXpath($dom);
		$result = $xpath->query($path);
		
		if ($result->length > 0) {
			return $result->item(0)->nodeValue;	
		}else return "Not Found!";
	}
	
	
	public function creatResources($body,$find,$k,$userName,$pass,$customerID,$amount){
		
		$postParams='';
		if($find[1]){
			$viewState=$this->htmlParse($body,'//input[@id="__VIEWSTATE"]/@value');
			logData("log_$devid.txt","extrected params $k::viewState::".$viewState);
			$eventValidation=$this->htmlParse($body,'//input[@id="__EVENTVALIDATION"]/@value');
			logData("log_$devid.txt","extrected params $k::Eventvalidation::".$eventValidation);
		}
		
		if(isset($viewState))
			$viewState=urlencode($viewState);
		if(isset($eventValidation))
			$eventValidation=urlencode($eventValidation);
		
		if($k===0)
			$postParams="__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$viewState&__VIEWSTATEGENERATOR=D4454486&__EVENTVALIDATION=$eventValidation&ctl00%24CenterCPH%24tbUsername=$userName&ctl00%24CenterCPH%24tbPassword=$pass&ctl00%24CenterCPH%24btnSubmit=Login&ctl00%24CenterCPH%24HiddenField1=";//_Login
		elseif ($k===3)
			$postParams="__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$viewState&__VIEWSTATEGENERATOR=61276DEF&__EVENTVALIDATION=$eventValidation&ctl00%24CenterCPH%24tbCustomerID=$customerID&ctl00%24CenterCPH%24btnGetCustomer=Get+Customer&ctl00%24CenterCPH%24HiddenField1=";//_getcustomer
		elseif ($k===4)
			$postParams="__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=$viewState&__VIEWSTATEGENERATOR=61276DEF&__EVENTVALIDATION=$eventValidation&ctl00%24CenterCPH%24tbAmount=$amount&ctl00%24CenterCPH%24btnSubmit=Submit&ctl00%24CenterCPH%24HiddenField1=";//Recharge
		logData("log_$devid.txt","return param::$i::".$postParams);
		return $postParams;
		
	}
	//getExpected($i,$expect,$cookie,$session,$location,$userName,$pass,$customerID,$amount);
	public function getExpected($i,$expect,$cookie,$sessionID,$loc,$useName,$pass,$customerID,$amount,$len){
		
		$expected = array(
				0 => array(//Load LOGIN PAGE
						'type' => 'GET',
						'expected' => "http://www.softwareworkshop.net/",
						'url' => "http://pcs.d2h.com:8080/Terminals/Login.aspx",
						'xPath' => array(
								'//a/@href',
								'true'
						),
						'header' => array(
								"Host: pcs.d2h.com:8080",
								"Referer: http://pcs.d2h.com:8080/Terminals/Logout.aspx?STATUS=7.2",
								"User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0",
								"Content-Type: application/x-www-form-urlencoded"
						)
				),
				1 => array(//LOGIN PAGE
						'type' => 'POST',
						'expected' => "/Terminals/Default.aspx",
						'url' => "http://pcs.d2h.com:8080/Terminals/Login.aspx",
						'xPath' => array(
								'//a/@href',
								'false'
						),
						'header' => array(
								"Host: pcs.d2h.com:8080",
								"Referer: http://pcs.d2h.com:8080/Terminals/Login.aspx",
								"User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0",
								"Content-Length: $len",
								"Content-Type: application/x-www-form-urlencoded"
						)
				),
				2 => array(//Profile Page
						'type' => 'GET',
						'expected' => "Profile Information",
						'url' => "http://pcs.d2h.com:8080/Terminals/Default.aspx",
						'xPath' => array(
								'//ul[@id="side-bar"]/li',
								'false'
						),
						'header' => array(
								'Host: pcs.d2h.com:8080',
								'Connection: keep-alive',
								"Cookie: $cookie; $sessionID",
								'Referer: http://pcs.d2h.com:8080/Terminals/Login.aspx',
								'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0'
						)
				),
				
				3 => array(//customer found or not 
						'type' => 'POST',
						'expected' => "Get Customer",
						'url' => "http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx",
						'xPath' => array(
								'//input[@type="submit"]/@value',
								'true'
						),
						'header' => array(
								"Cookie: $cookie; $sessionID",
								"Host: pcs.d2h.com:8080",
								"Referer: http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx",
								"User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0",
								"Content-Length: $len",
								"Content-Type: application/x-www-form-urlencoded"
						)
				),
				4 => array(//customer found or not
						'type' => 'POST',
						'expected' => "$customerID",
						'url' => "http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx",
						'xPath' => array(
								"//input[@class='aspNetDisabled']/@value",
								 'true'
						),
						'header' => array(
								"Cookie: $cookie; $sessionID",
								"Host: pcs.d2h.com:8080",
								"Referer: http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx",
								"User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0",
								"Content-Length: $len",
								"Content-Type: application/x-www-form-urlencoded"
						)
				),
				5 => array(//Enter amount and call for recharge
						'type' => 'POST',
						'expected' => "/Terminals/RechargeResult.aspx",
						'url' => "http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx",
						'xPath' => array(
								"//a/@href",
								'false'
						),
						'header' =>  array(
								"Cookie: $cookie; $sessionID",
								"Host: pcs.d2h.com:8080",
								"Referer: http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx",
								"User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0",
								"Content-Length: $len",
								"Content-Type: application/x-www-form-urlencoded"
						)
				),
				6 => array(//Reloade the $loc to get message
						'type' => 'GET',
						'expected' => "Customer with Customer ID:",
						'url' => "http://pcs.d2h.com:8080".$loc,
						'xPath' => array(
								'//div[@class="message"][@id="message"]',
								'true'
						),
						'header' => array(
								'Host: pcs.d2h.com:8080',
								//'Connection: keep-alive',
								"Cookie: $cookie; $sessionID",
								'Referer: http://pcs.d2h.com:8080/Terminals/CustomerRecharge.aspx',
								'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0'
						)
				),
				
				7 => array(//LOgout
						'type' => 'GET',
						'expected' => "",
						'url' => 'http://pcs.d2h.com:8080/Terminals/Logout.aspx?STATUS=7.2',
						'xPath' => array(
								'//div[@class="message"][@id="message"]',
								'false'
						),
						'header' => array(
								'Host: pcs.d2h.com:8080',
								//'Connection: keep-alive',
								"Cookie: $cookie; $sessionID",
								'Referer: http://pcs.d2h.com:8080/Terminals/Default.aspx',
								'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0'
						)
				)
				
		);
		return $expected[$i];
	}
	
}

?>