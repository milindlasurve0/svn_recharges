<?php

include 'config.php';

if(date("H:i")=='03:00'){
	$shell_query = "/sbin/shutdown -r now";
	shell_exec($shell_query);
}

	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php command > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);

    $shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php cleanq > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
    
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php bulkupdate > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);

	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php balupdate > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php joinsms > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
    
    $ret5_shell_query = "nohup sh " . DOCUMENT_ROOT . "recharges/redis_process_definer.sh > /dev/null 2> /dev/null & echo $!";
    shell_exec($ret5_shell_query);

	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/backup.php > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);

	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php killidle > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	
	//Electricity
	if(ELECTRICITY){
		$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php ELECHECK > /dev/null 2> /dev/null & echo $!";
		shell_exec($shell_query);
	}
	//Internet
	if(INTERNET){
		$shell_query = "nohup python ".DOCUMENT_ROOT . "recharges/network_change.py > /dev/null 2> /dev/null & echo $!";
		shell_exec($shell_query);
	}

if(date("i")%5==0){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php closing > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php balcheck > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}
if(date("i")%7==0){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php SMSCOUNT > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}
if(date("i")==0){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/slaveRestore.php > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

if(date("i")%15==0){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php UPDATEOPERATOR > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php SALESIMS > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php autocheck > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);//
	
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php RANGEUPDATE > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);//RANGEUPDATE
}

if(date("i")%2==0){
	$shell_query = "nohup sh ".DOCUMENT_ROOT . "recharges/slavehealth.sh > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php VIDESIM > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

if(date("i")%10==0){
    $shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php negdiff > /dev/null 2> /dev/null & echo $!";
    shell_exec($shell_query);
}

//if(date("i")%3==0){
	$shell_query = "nohup sh ".DOCUMENT_ROOT . "recharges/healthcheck.sh > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
//}

//RESET limit today & roaming limit every 10 minutes
if(date("i")%10==0){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php limitupdate > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

if(date("H")%Time_Diff==0 && intval(date("i")) == intval(MACHINE_ID)%60 ){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php reloadtemps > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

//Mysql syncing with office server 
if(date("H")%Time_Diff==0 && intval(date("i"))%30 == intval(MACHINE_ID)%30 ){
    $shell_query = "nohup sh ".DOCUMENT_ROOT . "recharges/slave_sync.sh > /dev/null 2> /dev/null & echo $!";
	//$shell_query = "rsync -avz /var/lib/mysql_backup/ 182.74.214.234:/var/lib/mysql_".VENDOR_ID;
	shell_exec($shell_query);
}

//Code update 
if(intval(date("i"))%15 == intval(MACHINE_ID)%15 ){
    $shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php CODEUPDATE > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}


if(date("H:i")=='03:30'){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php reset > /dev/null 2> /dev/null & echo $!";
	//shell_exec($shell_query);
}
if(date("H:i")=='23:59'){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php GETSIM > /dev/null 2> /dev/null & echo $!";
	//shell_exec($shell_query);
}

if(date("H:i")=='10:01' && IDEAOTP){
    $shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php otpidea > /dev/null 2> /dev/null & echo $!";
    shell_exec($shell_query);
}

/*if(date("i")=='00:00' && date("H")%3 == 0){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php repair > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}*/


if(date("H:i")=='01:00'){
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php reloadtemps > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
	$shell_query = "nohup php ".DOCUMENT_ROOT . "recharges/start.php hide > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}
