<?php
include "config.php";

$ret = shell_exec("ps ax  | grep recharges/backup.php | wc -l");
if($ret > 4){
	exit;
}

$ret = shell_exec("ps ax  | grep recharges/restart.php | wc -l");
if($ret > 2){
	exit;
}

$ret1 = shell_exec("ps ax  | grep 'recharges/start.php start' | wc -l");
if($ret1 != 3){
	$processes = explode("\n",shell_exec("ps ax  | awk '/start.php start/ {print $1}'"));
	foreach($processes as $process){
		$process = trim($process);
		if(!empty($process))shell_exec("kill -9 $process");
	}
	
	shell_exec("nohup php ".DOCUMENT_ROOT."recharges/start.php start > /dev/null 2> /dev/null & echo $!");
	exit;
}

$ret2 = shell_exec("ps ax  | grep 'recharges/start.php receive' | wc -l");
if($ret2 != 3){
	$processes = explode("\n",shell_exec("ps ax  | awk '/start.php receive/ {print $1}'"));
	foreach($processes as $process){
		$process = trim($process);
		if(!empty($process))shell_exec("kill -9 $process");
	}
	
	$shell_query = "nohup php " . DOCUMENT_ROOT . "recharges/start.php receive > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

$ret3 = shell_exec("ps ax  | grep 'recharges/start.php recheck' | wc -l");
if($ret3 != 3){
	$processes = explode("\n",shell_exec("ps ax  | awk '/start.php recheck/ {print $1}'"));
	foreach($processes as $process){
		$process = trim($process);
		if(!empty($process))shell_exec("kill -9 $process");
	}
	
	$shell_query = "nohup php " . DOCUMENT_ROOT . "recharges/start.php recheck > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

$ret4 = shell_exec("ps ax  | grep 'recharges/start.php autocheck' | wc -l");
if($ret4 != 3){
	$processes = explode("\n",shell_exec("ps ax  | awk '/start.php autocheck/ {print $1}'"));
	foreach($processes as $process){
		$process = trim($process);
		if(!empty($process))shell_exec("kill -9 $process");
	}
	$shell_query = "nohup php " . DOCUMENT_ROOT . "recharges/start.php autocheck > /dev/null 2> /dev/null & echo $!";
	shell_exec($shell_query);
}

?>