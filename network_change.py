#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2012-2015 Matt Martz
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from subprocess import Popen, PIPE, STDOUT
import os
import re
import sys
import math
import signal
import socket
import timeit
import platform
import threading
import datetime
import subprocess
import time
import urllib
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

__version__ = '0.3.4'

# Some global variables we use
user_agent = None
source = None
shutdown_event = None
scheme = 'http'


# Used for bound_interface
socket_socket = socket.socket

try:
    import xml.etree.cElementTree as ET
except ImportError:
    try:
        import xml.etree.ElementTree as ET
    except ImportError:
        from xml.dom import minidom as DOM
        ET = None

# Begin import game to handle Python 2 and Python 3
try:
    from urllib2 import urlopen, Request, HTTPError, URLError
except ImportError:
    from urllib.request import urlopen, Request, HTTPError, URLError

try:
    from httplib import HTTPConnection, HTTPSConnection
except ImportError:
    e_http_py2 = sys.exc_info()
    try:
        from http.client import HTTPConnection, HTTPSConnection
    except ImportError:
        e_http_py3 = sys.exc_info()
        raise SystemExit('Your python installation is missing required HTTP '
                         'client classes:\n\n'
                         'Python 2: %s\n'
                         'Python 3: %s' % (e_http_py2[1], e_http_py3[1]))

try:
    from Queue import Queue
except ImportError:
    from queue import Queue

try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

try:
    from urlparse import parse_qs
except ImportError:
    try:
        from urllib.parse import parse_qs
    except ImportError:
        from cgi import parse_qs

try:
    from hashlib import md5
except ImportError:
    from md5 import md5

try:
    from argparse import ArgumentParser as ArgParser
except ImportError:
    from optparse import OptionParser as ArgParser

try:
    import builtins
except ImportError:
    def print_(*args, **kwargs):
        """The new-style print function taken from
        https://pypi.python.org/pypi/six/

        """
        fp = kwargs.pop("file", sys.stdout)
        if fp is None:
            return

        def write(data):
            if not isinstance(data, basestring):
                data = str(data)
            fp.write(data)

        want_unicode = False
        sep = kwargs.pop("sep", None)
        if sep is not None:
            if isinstance(sep, unicode):
                want_unicode = True
            elif not isinstance(sep, str):
                raise TypeError("sep must be None or a string")
        end = kwargs.pop("end", None)
        if end is not None:
            if isinstance(end, unicode):
                want_unicode = True
            elif not isinstance(end, str):
                raise TypeError("end must be None or a string")
        if kwargs:
            raise TypeError("invalid keyword arguments to print()")
        if not want_unicode:
            for arg in args:
                if isinstance(arg, unicode):
                    want_unicode = True
                    break
        if want_unicode:
            newline = unicode("\n")
            space = unicode(" ")
        else:
            newline = "\n"
            space = " "
        if sep is None:
            sep = space
        if end is None:
            end = newline
        for i, arg in enumerate(args):
            if i:
                write(sep)
            write(arg)
        write(end)
else:
    print_ = getattr(builtins, 'print')
    del builtins


class SpeedtestCliServerListError(Exception):
    """Internal Exception class used to indicate to move on to the next
    URL for retrieving speedtest.net server details

    """


def bound_socket(*args, **kwargs):
    """Bind socket to a specified source IP address"""

    global source
    sock = socket_socket(*args, **kwargs)
    sock.bind((source, 0))
    return sock


def distance(origin, destination):
    """Determine distance between 2 sets of [lat,lon] in km"""

    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) *
         math.cos(math.radians(lat2)) * math.sin(dlon / 2) *
         math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d


def build_user_agent():
    """Build a Mozilla/5.0 compatible User-Agent string"""

    global user_agent
    if user_agent:
        return user_agent

    ua_tuple = (
        'Mozilla/5.0',
        '(%s; U; %s; en-us)' % (platform.system(), platform.architecture()[0]),
        'Python/%s' % platform.python_version(),
        '(KHTML, like Gecko)',
        'speedtest-cli/%s' % __version__
    )
    user_agent = ' '.join(ua_tuple)
    return user_agent


def build_request(url, data=None, headers={}):
    """Build a urllib2 request object

    This function automatically adds a User-Agent header to all requests

    """

    if url[0] == ':':
        schemed_url = '%s%s' % (scheme, url)
    else:
        schemed_url = url

    headers['User-Agent'] = user_agent
    return Request(schemed_url, data=data, headers=headers)


def catch_request(request):
    """Helper function to catch common exceptions encountered when
    establishing a connection with a HTTP/HTTPS request

    """

    try:
        uh = urlopen(request)
        return uh, False
    except (HTTPError, URLError, socket.error):
        e = sys.exc_info()[1]
        return None, e


class FileGetter(threading.Thread):
    """Thread class for retrieving a URL"""

    def __init__(self, url, start):
        self.url = url
        self.result = None
        self.starttime = start
        threading.Thread.__init__(self)

    def run(self):
        self.result = [0]
        try:
            if (timeit.default_timer() - self.starttime) <= 10:
                request = build_request(self.url)
                f = urlopen(request)
                while 1 and not shutdown_event.isSet():
                    self.result.append(len(f.read(10240)))
                    if self.result[-1] == 0:
                        break
                f.close()
        except IOError:
            pass


def downloadSpeed(files, quiet=False):
    """Function to launch FileGetter threads and calculate download speeds"""

    start = timeit.default_timer()

    def producer(q, files):
        for file in files:
            thread = FileGetter(file, start)
            thread.start()
            q.put(thread, True)
            if not quiet and not shutdown_event.isSet():
                sys.stdout.write('.')
                sys.stdout.flush()

    finished = []

    def consumer(q, total_files):
        while len(finished) < total_files:
            thread = q.get(True)
            while thread.isAlive():
                thread.join(timeout=0.1)
            finished.append(sum(thread.result))
            del thread

    q = Queue(6)
    prod_thread = threading.Thread(target=producer, args=(q, files))
    cons_thread = threading.Thread(target=consumer, args=(q, len(files)))
    start = timeit.default_timer()
    prod_thread.start()
    cons_thread.start()
    while prod_thread.isAlive():
        prod_thread.join(timeout=0.1)
    while cons_thread.isAlive():
        cons_thread.join(timeout=0.1)
    return (sum(finished) / (timeit.default_timer() - start))


class FilePutter(threading.Thread):
    """Thread class for putting a URL"""

    def __init__(self, url, start, size):
        self.url = url
        chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        data = chars * (int(round(int(size) / 36.0)))
        self.data = ('content1=%s' % data[0:int(size) - 9]).encode()
        del data
        self.result = None
        self.starttime = start
        threading.Thread.__init__(self)

    def run(self):
        try:
            if ((timeit.default_timer() - self.starttime) <= 10 and
                    not shutdown_event.isSet()):
                request = build_request(self.url, data=self.data)
                f = urlopen(request)
                f.read(11)
                f.close()
                self.result = len(self.data)
            else:
                self.result = 0
        except IOError:
            self.result = 0


def uploadSpeed(url, sizes, quiet=False):
    """Function to launch FilePutter threads and calculate upload speeds"""

    start = timeit.default_timer()

    def producer(q, sizes):
        for size in sizes:
            thread = FilePutter(url, start, size)
            thread.start()
            q.put(thread, True)
            if not quiet and not shutdown_event.isSet():
                sys.stdout.write('.')
                sys.stdout.flush()

    finished = []

    def consumer(q, total_sizes):
        while len(finished) < total_sizes:
            thread = q.get(True)
            while thread.isAlive():
                thread.join(timeout=0.1)
            finished.append(thread.result)
            del thread

    q = Queue(6)
    prod_thread = threading.Thread(target=producer, args=(q, sizes))
    cons_thread = threading.Thread(target=consumer, args=(q, len(sizes)))
    start = timeit.default_timer()
    prod_thread.start()
    cons_thread.start()
    while prod_thread.isAlive():
        prod_thread.join(timeout=0.1)
    while cons_thread.isAlive():
        cons_thread.join(timeout=0.1)
    return (sum(finished) / (timeit.default_timer() - start))


def getAttributesByTagName(dom, tagName):
    """Retrieve an attribute from an XML document and return it in a
    consistent format

    Only used with xml.dom.minidom, which is likely only to be used
    with python versions older than 2.5
    """
    elem = dom.getElementsByTagName(tagName)[0]
    return dict(list(elem.attributes.items()))


def getConfig():
    """Download the speedtest.net configuration and return only the data
    we are interested in
    """
    config = {
                'client': '',
                'times': '',
                'download': '',
                'upload': ''}
    return config

    request = build_request('://www.speedtest.net/speedtest-config.php')
    uh, e = catch_request(request)
    if e:
        print_('Could not retrieve speedtest.net configuration: %s' % e)
        sys.exit(1)
    configxml = []
    while 1:
        configxml.append(uh.read(10240))
        if len(configxml[-1]) == 0:
            break
    if int(uh.code) != 200:
        return None
    uh.close()
    try:
        try:
            root = ET.fromstring(''.encode().join(configxml))
            config = {
                'client': root.find('client').attrib,
                'times': root.find('times').attrib,
                'download': root.find('download').attrib,
                'upload': root.find('upload').attrib}
        except AttributeError:  # Python3 branch
            root = DOM.parseString(''.join(configxml))
            config = {
                'client': getAttributesByTagName(root, 'client'),
                'times': getAttributesByTagName(root, 'times'),
                'download': getAttributesByTagName(root, 'download'),
                'upload': getAttributesByTagName(root, 'upload')}
    except SyntaxError:
        print_('Failed to parse speedtest.net configuration')
        sys.exit(1)
    del root
    del configxml
    return config


def closestServers(client, all=False):
    """Determine the 5 closest speedtest.net servers based on geographic
    distance
    """
    closest = [{'name': 'Ahemdabad', 'url': 'http://speedtest1.ishancloud.com/upload.php', 'country': 'India', 'lon': '72.5800', 'cc': 'IN', 'host': 'speedtest1.ishancloud.com:8080', 'sponsor': 'ISHAN Netsol Pvt Ltd', 'url2': 'http://speedtest2.ishancloud.com/upload.php', 'lat': '23.0300', 'id': '6328', 'd': 3.7734481470430428}]
    return closest
    urls = [
    '://www.speedtest.net/speedtest-servers-static.php',
        '://c.speedtest.net/speedtest-servers-static.php',
       # '://www.speedtest.net/speedtest-servers.php',
        #'://c.speedtest.net/speedtest-servers.php',
    ]
    errors = []
    servers = {}
    for url in urls:
        try:
            request = build_request(url)
            uh, e = catch_request(request)
            if e:
                errors.append('%s' % e)
                raise SpeedtestCliServerListError
            serversxml = []
            while 1:
                serversxml.append(uh.read(10240))
                if len(serversxml[-1]) == 0:
                    break
            if int(uh.code) != 200:
                uh.close()
                raise SpeedtestCliServerListError
            uh.close()
            try:
                try:
                    root = ET.fromstring(''.encode().join(serversxml))
                    elements = root.getiterator('server')
                except AttributeError:  # Python3 branch
                    root = DOM.parseString(''.join(serversxml))
                    elements = root.getElementsByTagName('server')
            except SyntaxError:
                raise SpeedtestCliServerListError
            for server in elements:
                try:
                    attrib = server.attrib
                except AttributeError:
                    attrib = dict(list(server.attributes.items()))
                d = distance([float(client['lat']),
                              float(client['lon'])],
                             [float(attrib.get('lat')),
                              float(attrib.get('lon'))])
                attrib['d'] = d
                if d not in servers:
                    servers[d] = [attrib]
                else:
                    servers[d].append(attrib)
            del root
            del serversxml
            del elements
        except SpeedtestCliServerListError:
            continue

        # We were able to fetch and parse the list of speedtest.net servers
        if servers:
            break

    if not servers:
        print_('Failed to retrieve list of speedtest.net servers:\n\n %s' %
               '\n'.join(errors))
        sys.exit(1)

    closest = []
    for d in sorted(servers.keys()):
        for s in servers[d]:
            closest.append(s)
            if len(closest) == 5 and not all:
                break
        else:
            continue
        break

    del servers
    print_(str(closest))
    return closest


def getBestServer(servers):
    """Perform a speedtest.net latency request to determine which
    speedtest.net server has the lowest latency
    """

    results = {}
    for server in servers:
        cum = []
        url = '%s/latency.txt' % os.path.dirname(server['url'])
        urlparts = urlparse(url)
        for i in range(0, 3):
            try:
                if urlparts[0] == 'https':
                    h = HTTPSConnection(urlparts[1])
                else:
                    h = HTTPConnection(urlparts[1])
                headers = {'User-Agent': user_agent}
                start = timeit.default_timer()
                h.request("GET", urlparts[2], headers=headers)
                r = h.getresponse()
                total = (timeit.default_timer() - start)
            except (HTTPError, URLError, socket.error):
                cum.append(3600)
                continue
            text = r.read(9)
            if int(r.status) == 200 and text == 'test=test'.encode():
                cum.append(total)
            else:
                cum.append(3600)
            h.close()
        avg = round((sum(cum) / 6) * 1000, 3)
        results[avg] = server
    fastest = sorted(results.keys())[0]
    best = results[fastest]
    best['latency'] = fastest
    #print_("timespppp");
    return best


def ctrl_c(signum, frame):
    """Catch Ctrl-C key sequence and set a shutdown_event for our threaded
    operations
    """

    global shutdown_event
    shutdown_event.set()
    raise SystemExit('\nCancelling...')


def version():
    """Print the version"""

    raise SystemExit(__version__)


def speedtest():
    """Run the full speedtest.net test"""

    global shutdown_event, source, scheme
    shutdown_event = threading.Event()

    signal.signal(signal.SIGINT, ctrl_c)

    description = (
        'Command line interface for testing internet bandwidth using '
        'speedtest.net.\n'
        '------------------------------------------------------------'
        '--------------\n'
        'https://github.com/sivel/speedtest-cli')

    parser = ArgParser(description=description)
    # Give optparse.OptionParser an `add_argument` method for
    # compatibility with argparse.ArgumentParser
    try:
        parser.add_argument = parser.add_option
    except AttributeError:
        pass
    parser.add_argument('--bytes', dest='units', action='store_const',
                        const=('byte', 1), default=('bit', 8),
                        help='Display values in bytes instead of bits. Does '
                             'not affect the image generated by --share')
    parser.add_argument('--share', action='store_true',
                        help='Generate and provide a URL to the speedtest.net '
                             'share results image')
    parser.add_argument('--simple', action='store_true',
                        help='Suppress verbose output, only show basic '
                             'information')
    parser.add_argument('--list', action='store_true',
                        help='Display a list of speedtest.net servers '
                             'sorted by distance')
    parser.add_argument('--server', help='Specify a server ID to test against')
    parser.add_argument('--mini', help='URL of the Speedtest Mini server')
    parser.add_argument('--source', help='Source IP address to bind to')
    parser.add_argument('--timeout', default=10, type=int,
                        help='HTTP timeout in seconds. Default 10')
    parser.add_argument('--secure', action='store_true',
                        help='Use HTTPS instead of HTTP when communicating '
                             'with speedtest.net operated servers')
    parser.add_argument('--version', action='store_true',
                        help='Show the version number and exit')

    options = parser.parse_args()
    if isinstance(options, tuple):
        args = options[0]
    else:
        args = options
    del options

    # Print the version and exit
    if args.version:
        version()

    socket.setdefaulttimeout(args.timeout)

    # Pre-cache the user agent string
    build_user_agent()

    # If specified bind to a specific IP address
    if args.source:
        source = args.source
        socket.socket = bound_socket

    if args.secure:
        scheme = 'https'

    if not args.simple:
        print_()
    try:
        config = getConfig()
    except URLError:
        print_('Cannot retrieve speedtest configuration')
        sys.exit(1)

    if not args.simple:
        print_()
    if args.list or args.server:
        servers = closestServers(config['client'], True)
        if args.list:
            serverList = []
            for server in servers:
                line = ('%(id)4s) %(sponsor)s (%(name)s, %(country)s) '
                        '[%(d)0.2f km]' % server)
                serverList.append(line)
            print_('\n'.join(serverList).encode('utf-8', 'ignore'))
            sys.exit(0)
    else:
        servers = closestServers(config['client'])


    if args.server:
        try:
            best = getBestServer(filter(lambda x: x['id'] == args.server,
                                        servers))
        except IndexError:
            print_('Invalid server ID')
            sys.exit(1)
    elif args.mini:
        name, ext = os.path.splitext(args.mini)
        if ext:
            url = os.path.dirname(args.mini)
        else:
            url = args.mini
        urlparts = urlparse(url)
        try:
            request = build_request(args.mini)
            f = urlopen(request)
        except:
            print_('Invalid Speedtest Mini URL')
            sys.exit(1)
        else:
            text = f.read()
            f.close()
        extension = re.findall('upload_extension: "([^"]+)"', text.decode())
        if not extension:
            for ext in ['php', 'asp', 'aspx', 'jsp']:
                try:
                    request = build_request('%s/speedtest/upload.%s' %
                                            (args.mini, ext))
                    f = urlopen(request)
                except:
                    pass
                else:
                    data = f.read().strip()
                    if (f.code == 200 and
                            len(data.splitlines()) == 1 and
                            re.match('size=[0-9]', data)):
                        extension = [ext]
                        break
        if not urlparts or not extension:
            print_('Please provide the full URL of your Speedtest Mini server')
            sys.exit(1)
        servers = [{
            'sponsor': 'Speedtest Mini',
            'name': urlparts[1],
            'd': 0,
            'url': '%s/speedtest/upload.%s' % (url.rstrip('/'), extension[0]),
            'latency': 0,
            'id': 0
        }]

        best = servers[0]
        #try:
         #   best = getBestServer(servers)
        #except:
        #    best = servers[0]
    else:
        #best = getBestServer(servers)
        best = {
         'url': 'http://speedtestahm.vodafone.ind.in/speedtest/upload.php'
        }


    sizes = [350]
    urls = []
    for size in sizes:
        for i in range(0, 1):
            urls.append('%s/random%sx%s.jpg' %
                        (os.path.dirname(best['url']), size, size))
    if not args.simple:
        print_('Testing download speed', end='')
    dlspeed = downloadSpeed(urls, args.simple)
    if not args.simple:
        print_()
    #print_('Download: %0.2f M%s/s' %((dlspeed / 1000 / 1000) * args.units[1], args.units[0]))

    sizesizes = [int(.25 * 1000 * 1000)]
    sizes = []
    for size in sizesizes:
        for i in range(0, 1):
            sizes.append(size)
    if not args.simple:
        print_('Testing upload speed', end='')
    ulspeed = uploadSpeed(best['url'], sizes, args.simple)
    if not args.simple:
        print_()
    #print_('Upload: %0.2f M%s/s' %((ulspeed / 1000 / 1000) * args.units[1], args.units[0]))

    
    dlspeedk = int(round((dlspeed / 1000) * 8, 0))
    ulspeedk = int(round((ulspeed / 1000) * 8, 0))
    
    print_('Download speed: %d Kbps' % dlspeedk)
    print_('Upload speed: %d Kbps' % ulspeedk)
    
    return [dlspeedk,ulspeedk]
        
        # Build the request to send results back to speedtest.net
        # We use a list instead of a dict because the API expects parameters
        # in a certain order
        

#
#    This Function is use to execute system command through python
#
def command(cmd):
    try:
        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = p.stdout.read()
        return output
    except Exception, e:
        print str(e)

#
#    This function is a utility to send mail through python
#
def email(sender,receivers,message):
    smtpObj = smtplib.SMTP("localhost")
    smtpObj.sendmail(sender, receivers, message)
    smtpObj.quit()
    return 1    


#
#    Custom mails function to send html in mail body
#
def mailAlert (toIds, htmlText,subject=None):
    msg = MIMEMultipart('alternative')
    if(subject != None):
        msg['Subject'] = subject
    else:
        msg['Subject'] = "Alert"
        msg['From']    = "network_monitor_script@mindarray.com"
        msg['To']      = ', '.join(toIds)
    alert_mailFrom = "system@mindsarray.com"
    HTML_BODY = MIMEText(htmlText, 'html')
    msg.attach(HTML_BODY)
    email(alert_mailFrom, toIds, msg.as_string())


#
#    This Function is use to get count of active connection
#    
def active_connection_count():
    cmd = 'nmcli dev | grep  " connected" | wc -l'
    result = command(cmd).strip()
    return result

    
#
#    This Function is use to check for internet connectivity and data loss
#    
def internet_status():
    ATTEMPT = 5
    cmd = """ping -c %s 8.8.8.8 | egrep 'received' | awk -F', ' '{print $2}' | awk '{ print $1}'"""%(str(ATTEMPT))
    try:
        cmd_result = int(command(cmd).strip())
    except:
        cmd_result = 0
    if (int(cmd_result) >= 4):
        result = True
    else:
        result = False
    print_("Internet ping result is ",cmd_result)    
    print_("Internet status is ",result)
    
    return result


#
#    This Function is to connect to a specific network 
#
def open_connection(con):
    cmd = """nmcli -t con up id %s"""%str(con) 
    cmd_result = command(cmd).strip()
    if(con==secondary_con):
		time.sleep(5)
    return cmd_result


#
#    This Function is to close particular network connection
#
def close_connection(con):
    cmd = "nmcli -t con down id %s"%str(con)
    #print "command to close connection : "+str(cmd)
    cmd_result = command(cmd).strip()
    return cmd_result

#
#    This Function get currently connected connections
#    
def get_connected_con():
    print "getting connected connections"
    cmd = """nmcli connection status | awk -F' ' '{print $1}' | awk 'END{print}'"""
    cmd_result = command(cmd).strip('\n').split('\n')
    print "Current connection is: " + str(cmd_result)
    return cmd_result

#
# This Function prevent multiple network connection
#
def control_multiple_connection():
    # set it to "eth0" if primary interface is LAN and if its "wifi" keep it eth1 or wlan0. check it using "ifconfig command"
    default_con1 = lan_con    
    # This is for connection through Data Card
    default_con2 = data_con
    con_list = get_connected_con
    print "total connection "+str(len(con_list))
    if(len(con_list) > 1):
        if (default_con1 not in con_list):
            default_con1 = default_con2
        print "default connection "+str(default_con1)    
        for con in con_list:
            if (con == default_con1):
                print "connection not to close "+str(con)
                pass
            else:
                print "connection to close "+str(con)
                close_connection(con)

#
#  This function provide mail content and other parameter to mail function
#
def sendmail(con=""):
    toIds = ['chetan@mindsarray.com','ashish@mindsarray.com'] # email ids to whom mail need to be triggered
    htmlText = "System is switching its network connection to : "+str(con)
    subject = "modem network switch alert"
    try:
        mailAlert (toIds, htmlText,subject=None)
    except:
        pass
    
#
# This function will swithch the connection to new_conn & if new_conn doesn't work then it will switch it back to old_conn
#    
def switch_connection(old_conn,new_conn):
    print_('Closing connection:',old_conn)
    close_connection(old_conn)
    print_('Opening connection:',new_conn)
    open_connection(new_conn)
    
    internet_check = internet_status()
    print_('Internet status for the new connection:',internet_check)
    
    if(internet_check == False):
        print_('New connection is not working so Closing it:',new_conn)
        close_connection(new_conn)
        print_('New connection is not working so opening old connection:',old_conn)
        open_connection(old_conn)
        return False
    else:
        return True
        
def down_interface(conn):
	cmd = "ifconfig eth0 down"
	command(cmd).strip()

def up_interface(conn):
        cmd = "ifconfig eth0 up"
        command(cmd).strip()
	time.sleep(5)

def network_switch(connection,type):
    if(connection == primary_con):
        connection_next = secondary_con
    else:
        connection_next = primary_con
    if(type == 0):
        switch_connection(connection,connection_next)
    elif(type == 1):
        speed_res = speedtest()
        download = speed_res[0]
        upload = speed_res[1]
        
        if(download < download_threshold or upload < download_threshold):
            switch_connection(connection,connection_next)
    else:
        result = switch_connection(connection,connection_next)
        if(result == True):
            speed_res = speedtest()
            download = speed_res[0]
            upload = speed_res[1]
            
            if(download < download_threshold or upload < download_threshold):
                switch_connection(connection_next,connection)
                
            


def main():
    try:
        print time.strftime("%Y-%m-%d %H:%M:%S")
        conn = get_connected_con()
        if(conn[0] == 'NAME'): #IF nothing is connected
           connect = switch_connection(secondary_con,primary_con)
           if(connect == True):
               conn[0] = primary_con
           else:
               conn[0] = secondary_con    
            
        if(int(time.strftime("%M")) == 0): #check every hour if primary connection is working or not
            if(conn[0] != primary_con):
                network_switch(conn[0],2)
            else:
                network_switch(conn[0],1)
        elif (int(time.strftime("%M")) % 15 == 0): #speed check every 15 mins to ensure proper internet connectivity. Switch will also happen here is speed is not right
            network_switch(conn[0],1)
        else:  #Normal ping checking every minute to understand whether internet is working or not. Switch if not working
            internet_check = internet_status()
            if(internet_check == False):
                network_switch(conn[0],0)
        
    except KeyboardInterrupt:
        print_('\nCancelling...')


if __name__ == '__main__':
    global primary_con, secondary_con, download_threshold, upload_threshold
    cmd = """cat sysconfig.php | grep -i "PRIMARY_CON"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep PRIMARY_CON | sed 's/PRIMARY_CON//g'"""
    pri_result = command(cmd)
    if(pri_result == ""):
        primary_con = "Wired connecton 1"
    else:
        primary_con = pri_result
    print_('Primary connection:',primary_con)
    cmd = """cat sysconfig.php | grep -i "SECONDARY_CON"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep SECONDARY_CON | sed 's/SECONDARY_CON//g'"""
    sec_result = command(cmd)
    if(sec_result == ""):
        secondary_con = "Pay1Wifi"
    else:
        secondary_con = sec_result
    
    cmd = """cat sysconfig.php | grep -i "DOWNLOAD_THRESOLD"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep DOWNLOAD_THRESOLD | sed 's/DOWNLOAD_THRESOLD//g'"""
    down_result = command(cmd)
    if(pri_result == ""):
        download_threshold = 300;
    else:
        download_threshold = down_result
    cmd = """cat sysconfig.php | grep -i "UPLOAD_THRESOLD"  | awk -F'(' '{print $2}' | tr -d "');" |  awk -F',' '{ print $1 $2}' | grep UPLOAD_THRESOLD | sed 's/UPLOAD_THRESOLD//g'"""
    upload_result = command(cmd)
    if(upload_result == ""):
        upload_threshold = 100
    else:
        upload_threshold = upload_result
    main()

# vim:ts=4:sw=4:expandtab