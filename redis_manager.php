<?php

require("Predis/Autoloader.php");

$default_server_config = array(
        "scheme" => "tcp",
        "host" => "107.22.176.158",
        "port" => 6300,
        "password"=>'$avail$p@y!'
);
require_once 'sysconfig.php';

$inComming_Q = VENDOR_ID."_".MACHINE_ID;
$outGoing_Q = "dynamic_service";

class redisManager{

     public function __construct() {
        try{	    
            //global $default_server_config;
            Predis\Autoloader::register();
            $this->redisObj = new Predis\Client(array(
        "scheme" => "tcp",
        "host" => "107.22.176.158",
        "port" => 6300,
        "password"=>'$avail$p@y!'
));
        }  catch (Exception $e){
                echo "Couldn't connected to Redis";
                echo $e->getMessage();
                $this->notifyAlert($e->getMessage());
        }
    }
	
    public function notifyAlert($msg){
	echo $msg;	
    }

    public function fetch_request(){
	global $inComming_Q ;
	$request = $this->redisObj->rpop($inComming_Q);
	$key = "activestatus_".VENDOR_ID;
	$this->redisObj->setex($key,5,1);
	return $request;
    }
    
	public function fetch_request_prev(){
	$request = $this->redisObj->rpop("updateIncoming_".VENDOR_ID);
	return $request;
    }

    public function send_response($data=null){
        global $outGoing_Q ;
        $this->redisObj->hset($outGoing_Q,$data['uuid'],$data['response']);
    }
    
    public function check_request_validity($uuid){
        return $this->redisObj->exists($uuid);
    }
    
	public function remove_txn($opr,$txnid){
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3','181'=>'18');
        $opr_new = isset($arr_map[$opr])?$arr_map[$opr]:$opr;
    	$queue = "Trans_". VENDOR_ID."_".$opr_new;
    	$this->redisObj->hdel($queue,$txnid);
    }
    
	public function add_txn($opr,$txnid){
		$arr_map = array('7'=>'8','10'=>'9','27'=>'9','28'=>'12','29'=>'11','31'=>'30','34'=>'3','181'=>'18');
        $opr_new = isset($arr_map[$opr])?$arr_map[$opr]:$opr;
    	$queue = "Trans_". VENDOR_ID."_".$opr_new;
    	$this->redisObj->hset($queue,$txnid,time()+120);
    }
    
	public function set_queue_length($opr,$length){
		$key = "queue_".VENDOR_ID."_".$opr;
    	$this->redisObj->set($key,$length);
    	$this->redisObj->expire($key,120);
    }
    
    public function check_keys($cmd) {
        return $this->redisObj->keys($cmd);
    }

    public function get_hash_data($hashname){
        return $this->redisObj->hgetall($hashname);
    }

    public function expire_txn_from_hash($hashname,$key){
        return $this->redisObj->hdel($hashname,$key);
    }

    public function send_modem_updates($request_id,$data){
        $handler_Q = "UPDATE_HANDLER_Q";
        $RESPONSE_HASH = "modems_response_data";
        $this->redisObj->hset($RESPONSE_HASH,$request_id,  json_encode($data));
        $this->redisObj->lpush($handler_Q,$request_id);
        return;
    }
    
}

?>
